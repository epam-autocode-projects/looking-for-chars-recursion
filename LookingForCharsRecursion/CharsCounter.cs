﻿using System;

namespace LookingForCharsRecursion
{
    public static class CharsCounter
    {
        /// <summary>
        /// Searches a string for all characters that are in <see cref="Array" />, and returns the number of occurrences of all characters.
        /// </summary>
        /// <param name="str">String to search.</param>
        /// <param name="chars">One-dimensional, zero-based <see cref="Array"/> that contains characters to search for.</param>
        /// <returns>The number of occurrences of all characters.</returns>
        private static int count = 0;

        public static int GetCharsCount(string str, char[] chars)
        {
            if (str is null || chars is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (str.Length == 0)
            {
                return 0;
            }

            count = GetChars(str, chars, 0, 0, 0);
            return count;
        }

        public static int GetChars(string str, char[] chars, int j, int i, int k)
        {
            if (str is null || chars is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (j == chars.Length - 1 && i == str.Length - 1)
            {
                return k;
            }
            else if (str[i] == chars[j] && i < str.Length)
            {
                k++;
            }
            else if (i == str.Length - 1)
            {
                i = -1;
                j++;
            }

            i++;

            return GetChars(str, chars, j, i, k);
        }

        /// <summary>
        /// Searches a string for all characters that are in <see cref="Array" />, and returns the number of occurrences of all characters within the range of elements in the <see cref="string"/> that starts at the specified index and ends at the specified index.
        /// </summary>
        /// <param name="str">String to search.</param>
        /// <param name="chars">One-dimensional, zero-based <see cref="Array"/> that contains characters to search for.</param>
        /// <param name="startIndex">A zero-based starting index of the search.</param>
        /// <param name="endIndex">A zero-based ending index of the search.</param>
        /// <returns>The number of occurrences of all characters within the specified range of elements in the <see cref="string"/>.</returns>
        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex)
        {
            if (str is null || chars is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (startIndex < 0 || endIndex < 0 || startIndex > endIndex || startIndex > str.Length || endIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(str));
            }

            if (str.Length == 0)
            {
                return 0;
            }

            if (str.Length == 0)
            {
                return 0;
            }

            count = GetCharsStart(str, chars, 0, startIndex, 0, startIndex, endIndex);
            return count;
        }

        public static int GetCharsStart(string str, char[] chars, int j, int i, int k, int start, int end)
        {
            if (str is null || chars is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (j == chars.Length - 1 && i == end)
            {
                if (str[i] == chars[j])
                {
                    k++;
                }

                return k;
            }
            else if (str[i] == chars[j] && i <= end)
            {
                k++;
            }

            if (i == end)
            {
                i = start;
                j++;
            }

            i++;

            return GetCharsStart(str, chars, j, i, k, start, end);
        }

        /// <summary>
        /// Searches a string for a limited number of characters that are in <see cref="Array" />, and returns the number of occurrences of all characters within the range of elements in the <see cref="string"/> that starts at the specified index and ends at the specified index.
        /// </summary>
        /// <param name="str">String to search.</param>
        /// <param name="chars">One-dimensional, zero-based <see cref="Array"/> that contains characters to search for.</param>
        /// <param name="startIndex">A zero-based starting index of the search.</param>
        /// <param name="endIndex">A zero-based ending index of the search.</param>
        /// <param name="limit">A maximum number of characters to search.</param>
        /// <returns>The limited number of occurrences of characters to search for within the specified range of elements in the <see cref="string"/>.</returns>
        public static int GetCharsCount(string str, char[] chars, int startIndex, int endIndex, int limit)
        {
            if (str is null || chars is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if (startIndex < 0 || endIndex < 0 || startIndex > endIndex || startIndex > str.Length || endIndex > str.Length)
            {
                throw new ArgumentOutOfRangeException(nameof(str));
            }

            if (limit < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(limit));
            }

            if (str.Length == 0)
            {
                return 0;
            }

            count = GetCharsStartLim(str, chars, 0, startIndex, 0, startIndex, endIndex, limit);
            return count;
        }

        public static int GetCharsStartLim(string str, char[] chars, int j, int i, int k, int start, int end, int lim)
        {
            if (str is null || chars is null)
            {
                throw new ArgumentNullException(nameof(str));
            }

            if ((j == chars.Length - 1 && i == end) || k == lim)
            {
                if (str[i] == chars[j])
                {
                    k++;
                }

                return k;
            }
            else if (str[i] == chars[j] && i <= end)
            {
                k++;
            }

            if (i == end)
            {
                i = start;
                j++;
            }

            i++;

            return GetCharsStartLim(str, chars, j, i, k, start, end, lim);
        }
    }
}
